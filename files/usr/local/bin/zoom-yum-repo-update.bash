#!/usr/bin/env bash
set -euo pipefail

rpm_url=$(curl -sI https://zoom.us/client/latest/zoom_x86_64.rpm | grep '^location: ' | tr -d '\r' | sed 's/location:[[:space:]]//')
zoom_version=$(basename "$(dirname "${rpm_url}")")

# Check if the rpm was already downloaded
if ! ls -1 /var/lib/zoom-yum-repo/*"${zoom_version}-1"*.rpm; then
    echo "Downloading new version: ${zoom_version}"

    # Download the rpm
    tmp_rpm="/var/tmp/zoom-${zoom_version}.x86_64.rpm"
    curl -sSL --output "${tmp_rpm}" "${rpm_url}"

    # Find out what the actual version is
    rpm_version=$(rpm --query "${tmp_rpm}")

    # Add the new version to the yum repo and preserve destination SELinux context
    cp "${tmp_rpm}" "/var/lib/zoom-yum-repo/${rpm_version}.rpm"
    rm "${tmp_rpm}"
fi

# Update the yum repo data
echo "Updating yum repo data"
createrepo_c /var/lib/zoom-yum-repo/
