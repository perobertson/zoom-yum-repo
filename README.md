# zoom-yum-repo

This will create a local yum repo for Zoom.
It also creates an update service to keep Zoom up to date.

## Requirements

- basename
- createrepo_c
- curl
- rpm

## Usage

```bash
sudo make install
```
