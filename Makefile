.DEFAULT_GOAL := install

/etc/yum.repos.d/zoom-local.repo: files/etc/yum.repos.d/zoom-local.repo
/etc/yum.repos.d/zoom-local.repo: /var/lib/zoom-yum-repo
	install -m 0644 \
		files/etc/yum.repos.d/zoom-local.repo \
		/etc/yum.repos.d/zoom-local.repo

/usr/lib/systemd/system-preset/10-zoom-yum-repo-update.preset: files/usr/lib/systemd/system-preset/10-zoom-yum-repo-update.preset
/usr/lib/systemd/system-preset/10-zoom-yum-repo-update.preset: /usr/lib/systemd/system/zoom-yum-repo-update.timer
	install -m 0644 \
		files/usr/lib/systemd/system-preset/10-zoom-yum-repo-update.preset \
		/usr/lib/systemd/system-preset/10-zoom-yum-repo-update.preset
	systemctl daemon-reload

/usr/lib/systemd/system/zoom-yum-repo-update.service: files/usr/lib/systemd/system/zoom-yum-repo-update.service
/usr/lib/systemd/system/zoom-yum-repo-update.service: /usr/local/bin/zoom-yum-repo-update.bash
	install -m 0644 \
		files/usr/lib/systemd/system/zoom-yum-repo-update.service \
		/usr/lib/systemd/system/
	systemctl daemon-reload

/usr/lib/systemd/system/zoom-yum-repo-update.timer: files/usr/lib/systemd/system/zoom-yum-repo-update.timer
/usr/lib/systemd/system/zoom-yum-repo-update.timer: /usr/lib/systemd/system/zoom-yum-repo-update.service
	install -m 0644 \
		files/usr/lib/systemd/system/zoom-yum-repo-update.timer \
		/usr/lib/systemd/system/
	systemctl daemon-reload

/usr/local/bin/zoom-yum-repo-update.bash: files/usr/local/bin/zoom-yum-repo-update.bash
/usr/local/bin/zoom-yum-repo-update.bash: /var/lib/zoom-yum-repo
	install -m 0755 \
		files/usr/local/bin/zoom-yum-repo-update.bash \
		/usr/local/bin/

/var/lib/zoom-yum-repo:
	mkdir -p /var/lib/zoom-yum-repo

.PHONY: install
install:
	$(MAKE) install-service
	$(MAKE) start-service
	while systemctl is-active zoom-yum-repo-update.service; do sleep 5; done
	$(MAKE) install-zoom

.PHONY: install-repo
install-repo: /etc/yum.repos.d/zoom-local.repo

.PHONY: install-script
install-script: /usr/local/bin/zoom-yum-repo-update.bash

.PHONY: install-service
install-service: install-repo
	$(MAKE) /usr/lib/systemd/system-preset/10-zoom-yum-repo-update.preset
	systemctl preset zoom-yum-repo-update.timer

.PHONY: install-zoom
install-zoom:
	dnf makecache --repo zoom-local
	dnf install zoom
	dnf upgrade zoom

.PHONY: start-service
start-service:
	systemctl start zoom-yum-repo-update.service

.PHONY: enable-timer
enable-timer:
	systemctl enable zoom-yum-repo-update.timer

.PHONY: start-timer
start-timer:
	systemctl start zoom-yum-repo-update.timer
